/* Written by Daniel Rodriguez - 2020
 * Its purpose is to make weather stations that helps detecting
 * air and climate variables in the cities that can affect
 * our quality of life.
 */
//It uses the humidity and temperature sensor DTH11 and the MQ4 gas sensor
#include <DHT.h>//Library for using DHT sensor 
// It uses the "STM32LowPower" library to save power consuption

// *************  IMPORTANT: YOU HAVE TO PRESS RESET BUTTON THE FIRST TIME IS PLUGGED ON (for the sleeping mode) ****************
#include "STM32LowPower.h"

//Variables of DTH11
#define DHTPIN PA1 
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE); //initilize object dht for class DHT with DHT pin with STM32 and DHT type as DHT11

void setup() {
  //Begin serial communication
  Serial.begin(9600);
  dht.begin(); //Begins to receive Temperature and humidity values.
  pinMode(PA0, INPUT_ANALOG); //Declares PA0 for analog readings of the MQ4 
  LowPower.begin(); //Begin the power saving mode
}

void loop() {
  //Gets DTH11 values and prints info in serial console
  Serial.print(dht.readHumidity());
  Serial.print(F(" , "));
  Serial.print(dht.readTemperature());
  Serial.print(F(" , "));
  Serial.println(analogRead(PA0)); //Print MQ4 readings
  delay(1500); // waits a little for getting the readings
  LowPower.deepSleep(4000); // zzzz... for 4 seconds
}
